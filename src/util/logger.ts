import { white, blue, green, yellow, red, redBright, Chalk } from "chalk";

const log =
  (color: Chalk) =>
  (message: string, ...args: any[]) =>
    console.log(color(message), ...args);

const logger = {
  log: log(white),
  info: log(blue),
  success: log(green),
  warning: log(yellow),
  error: log(red),
  fatal: log(redBright),
};

export default logger;
