#!/usr/bin/env node
import prompts from "prompts";

(async () => {
  const response = await prompts({
    type: "text",
    name: "value",
    message: "What's your name?",
    validate: (value) => (value.length ? "Requires a name" : true),
  });

  console.log(response);
})();
